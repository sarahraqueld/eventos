import java.util.Scanner;

import br.ufrn.imd.eventos.controllers.EventoMBean;
import br.ufrn.imd.eventos.controllers.UsuarioMBean;
import br.ufrn.imd.eventos.dominio.Usuario;

public class App {
	
	public static void main(String[] args) {

		EventoMBean eventoMBean = new EventoMBean();
		UsuarioMBean usuarioMBean = new UsuarioMBean();
		Usuario usuarioLogado = null;
		
		Scanner s = new Scanner(System.in);
		String entrada = "" ;
		while(usuarioLogado == null){
			System.out.println("Insira nome de usuário para fazer login.");
			entrada = s.next();
			usuarioLogado = usuarioMBean.login(entrada);
			if(usuarioLogado == null)
				System.out.println("Usuário não existe, tente novamente.");
		}
		
		while(!entrada.equalsIgnoreCase("sair")){
			System.out.println("Escolha uma opção");
			System.out.println("1 - Eventos\n2 - Usuários");
			entrada = s.next();
			if(entrada.equals("1")){
				System.out.println("1 - Criar \n2 - Listar \n3 - Procurar por Id \n4 - Editar por Id \n5 - Remover por Id \n6- Participar de evento");
				entrada = s.next();
				switch (entrada) {
				case "1":
					System.out.println("Insira o título");
					entrada = s.next();
					eventoMBean.addEvento(entrada);
					break;
				case "2":
					eventoMBean.list();
					break;
				case "3":
					System.out.println("Insira o id para procurar um evento");
					entrada = s.next();
					eventoMBean.getById(entrada);
					break;
				case "4":
					System.out.println("Insira o id para editar um evento");
					entrada = s.next();
					System.out.println("Editando evento: ");
					if(eventoMBean.getById(entrada)){
						System.out.println("Novo título:");
						String titulo = s.next();
						System.out.println("Nova descrição:");
						String descricao = s.next();
						eventoMBean.edit(entrada, titulo, descricao);
					}
					break;
				case "5":
					System.out.println("Insira o id para remover um evento");
					entrada = s.next();
					eventoMBean.delete(entrada);
					break;
				case "6":
					System.out.println("Insira o id para se inscrever em um evento");
					entrada = s.next();
					eventoMBean.subscribe(entrada, usuarioLogado);
					break;
				default:
					break;
				}
			}
			else if(entrada.equals("2")){
				System.out.println("Menu de Usuário");
				System.out.println("1 - Criar \n 2 - Listar \n 3 - Procurar por Id \n 4 - Editar por Id \n 5 - Remover por Id");
				entrada = s.next();
				switch (entrada) {
				case "1":
					System.out.println("Insira o nome");
					entrada = s.next();
					usuarioMBean.add(entrada);
					break;
				case "2":
					usuarioMBean.list();
					break;
				case "3":
					System.out.println("Insira o id para procurar um usuario");
					entrada = s.next();
					usuarioMBean.getById(entrada);
					break;
				case "4":
					System.out.println("Insira o id para editar um usuario");
					entrada = s.next();
					System.out.println("Editando Usuário: ");
					if(usuarioMBean.getById(entrada)){
						System.out.println("Novo título:");
						String nome = s.next();
						System.out.println("Nova descrição:");
						String username = s.next();
						usuarioMBean.edit(entrada, nome, username);
					}
					break;
				case "5":
					System.out.println("Insira o id para remover um user");
					entrada = s.next();
					usuarioMBean.delete(entrada);
					break;
				default:
					break;
				}
				
			}
			else{
				System.out.println("Opção inválida");
			}
		}
		
		System.out.println("Encerrando programa");
	}
	

}
