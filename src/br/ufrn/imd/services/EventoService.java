package br.ufrn.imd.services;

import java.util.ArrayList;
import java.util.List;

import br.ufrn.imd.eventos.dao.EventoDao;
import br.ufrn.imd.eventos.dominio.Evento;
import br.ufrn.imd.eventos.dominio.Usuario;

public class EventoService {


	private static EventoDao eventoDao = new EventoDao();
	
	 
	public EventoService(){}
	
	public Evento getById(int id) {
		return eventoDao.getById(id);
	}
	
	public void addEvento(Evento e){
		// TOdo: Verificar regras de negócio antes de adicionar
		if(e.getTitulo().isEmpty()){
			System.out.println("Não é possível inserir");
			return;
		}
		eventoDao.add(e);
	}
	
	public Boolean subscribe(Evento e, Usuario u){
		eventoDao.subscribe(e, u);
		return true;
	}

	public List<Evento> listar() {
		return eventoDao.getAll();
		
	}

	public Boolean delete(int id) {
		if(eventoDao.delete(id))
			return true;
		else
			return false;
	}

	public Evento edit(int id, String titulo, String descricao) {
		return eventoDao.edit(id, titulo, descricao);
	
	}
	
	
}
