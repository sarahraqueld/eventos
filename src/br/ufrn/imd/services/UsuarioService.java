package br.ufrn.imd.services;

import java.util.ArrayList;
import java.util.List;

import br.ufrn.imd.eventos.dao.EventoDao;
import br.ufrn.imd.eventos.dao.UsuarioDao;
import br.ufrn.imd.eventos.dominio.Evento;
import br.ufrn.imd.eventos.dominio.Usuario;

public class UsuarioService {


	private static UsuarioDao usuarioDao = new UsuarioDao();
	
	 
	public UsuarioService(){}
	
	public Usuario getById(int id) {
		return usuarioDao.getById(id);
	}
	
	public Usuario getByUsername(String username) {
		return usuarioDao.getByUsername(username);
	}
	
	public Boolean add(Usuario u){
		// TOdo: Verificar regras de negócio antes de adicionar
		if(u.getNome().isEmpty()){
			System.out.println("Não é possível inserir");
			return false;
		}
		usuarioDao.add(u);
		return true;
	}

	public List<Usuario> listar() {
		return usuarioDao.getAll();
		
	}

	public Boolean delete(int id) {
		if(usuarioDao.delete(id))
			return true;
		else
			return false;
	}

	public Usuario edit(int id, String nome, String username) {
		return usuarioDao.edit(id, nome, username);
	}
	
	
}
