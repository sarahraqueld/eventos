package br.ufrn.imd.eventos.dominio;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

/**
 * Entidade que corresponde aos Eventos.
 * 
 * @author sarah
 *
 */

public class Evento{
	/**
	 * ID da entidade
	 */
	private int id;
	private String titulo;
	private String descricao;
	private String tipo;
	private Date dataInicio;
	private Date dataFim;
	private List<Usuario> participantes;
	//private Local local;
	//private Organizador organizador;
	
	public Evento(){}
	public Evento(int id, String titulo, String descricao, String tipo, Date dataInicio, Date dataFim) {
		this.id = id;
		this.titulo = titulo;
		this.descricao = descricao;
		this.tipo = tipo;
		this.dataInicio = dataInicio;
		this.dataFim = dataFim;
		this.participantes = new ArrayList<Usuario>();
	}
	
	public Evento(String titulo){
		this.titulo = titulo;
		this.participantes = new ArrayList<Usuario>();
	}

	
	public List<Usuario> getParticipantes() {
		return participantes;
	}
	public void setParticipantes(List<Usuario> participantes) {
		this.participantes = participantes;
	}
	
	public void addParticipante(Usuario u){
		this.participantes.add(u);
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public Date getDataInicio() {
		return dataInicio;
	}

	public void setDataInicio(Date dataInicio) {
		this.dataInicio = dataInicio;
	}

	public Date getDataFim() {
		return dataFim;
	}

	public void setDataFim(Date dataFim) {
		this.dataFim = dataFim;
	}
	
	
 	
}
