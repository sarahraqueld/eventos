package br.ufrn.imd.eventos.dominio;


import java.util.Date;

public class Usuario{
	
	private int id;
	private Boolean isAdmin;
	private int cpf;
	private String nome;
	private Date dataNascimento;
	private String email;
	private String username;
	private String senha;


	public Usuario(){
	}

	public Usuario(int id, Boolean isAdmin, int cpf, String nome, Date dataNascimento, String email, String username,
			String senha) {
		super();
		this.id = id;
		this.isAdmin = isAdmin;
		this.cpf = cpf;
		this.nome = nome;
		this.dataNascimento = dataNascimento;
		this.email = email;
		this.username = username;
		this.senha = senha;
	}

	public Usuario(String nome) {
		super();
		this.nome = nome;
	}
	
	

	public Usuario(int id, String nome, String username) {
		super();
		this.id = id;
		this.nome = nome;
		this.username = username;
	}

	public void setId(int id) {
		this.id = id;
	
	}
	public Boolean getIsAdmin() {
		return isAdmin;
	}

	public void setIsAdmin(Boolean isAdmin) {
		this.isAdmin = isAdmin;
	}

	public int getCpf() {
		return cpf;
	}

	public void setCpf(int cpf) {
		this.cpf = cpf;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Date getDataNascimento() {
		return dataNascimento;
	}

	public void setDataNascimento(Date dataNascimento) {
		this.dataNascimento = dataNascimento;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	public int getId() {
		return id;
	}

	

}