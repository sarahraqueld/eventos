package br.ufrn.imd.eventos.dominio;

import java.sql.Date;

public class SessaoTecnica extends Evento{
	
	private int id;
	private Date horaInicio;
	private Date horaFim;
	private Date dia;
	private int id_evento;

	public SessaoTecnica(){
		super();
	}

	public SessaoTecnica(int id, Date horaInicio, Date horaFim, Date dia, int id_evento) {
		super();
		this.id = id;
		this.horaInicio = horaInicio;
		this.horaFim = horaFim;
		this.dia = dia;
		this.id_evento = id_evento;
	}

}