package br.ufrn.imd.eventos.dominio;

/**
 * Entidade que corresponde aos Usuarios organizadores de um evento.
 * 
 * @author sarah
 */
public class Organizador extends Usuario{
	/**
	 * ID da entidade
	 */

	private int id;
	
 	/**
 	 * Construtor da classe
 	 */
 	public Organizador() {
	}
 	
	public Organizador(int id) {
		super();
		this.id = id;
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}
}
