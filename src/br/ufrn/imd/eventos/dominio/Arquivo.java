package br.ufrn.imd.eventos.dominio;
public class Arquivo{
	
	private int id;
	private String autor;
	private String link;
	private String nome;
	private String referencia;
	private int id_sessao;

	public Arquivo(){}

	public Arquivo(int id, String autor, String link, String nome,
	String referencia, int id_sessao){
		this.id = id;
		this.autor = autor;
		this.link = link;
		this.nome=nome;
		this.referencia = referencia;
		this.id_sessao = id_sessao;
	}
}