package br.ufrn.imd.eventos.dominio;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Entidade que corresponde aos Usuarios ouvintes de um evento.
 * 
 * @author sarah
 *
 */

public class Participante extends Usuario{
	/**
	 * ID da entidade
*/
	private int id;
	
 	/**
 	 * Construtor da classe
 	 */
 	public Participante() {
	}
 	
	public Participante(int id) {
		super();
		this.id = id;
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}
}
