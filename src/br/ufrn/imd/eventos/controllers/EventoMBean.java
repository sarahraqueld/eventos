package br.ufrn.imd.eventos.controllers;

import java.util.List;

import br.ufrn.imd.eventos.dominio.Evento;
import br.ufrn.imd.eventos.dominio.Usuario;
import br.ufrn.imd.services.EventoService;

public class EventoMBean{

	private static EventoService eventoService = new EventoService();
	
	private Evento eventoAtual;

	public void addEvento(String titulo) {
		Evento e = new Evento(titulo);
		System.out.println(e.getTitulo());
		eventoService.addEvento(e);
		System.out.println("Evento " + e.getTitulo() + " criado com id " + e.getId());
	}
	
	public Boolean subscribe(String id, Usuario u){
		getEventoById(id);
		return eventoService.subscribe(this.eventoAtual, u);
		
	}

	public void list() {
		List<Evento> eventos = eventoService.listar();
		for (Evento e : eventos) {
			System.out.println(e.getTitulo());
		}
	}
	
	private void getEventoById(String id){
		eventoAtual = eventoService.getById(Integer.parseInt(id));
	}
	
	public Boolean getById(String id){
		Evento e = eventoService.getById(Integer.parseInt(id));
		if(e != null){
			System.out.println("Evento: " + e.getTitulo());
			System.out.println("Participantes: ");
			for(Usuario u: e.getParticipantes() ){
				System.out.println(u.getNome());
			}
			return true;
		}
		else{
			System.out.println("Não foi possível encontrar evento com id " + id);
			return false;
		}
	}
	
	public void delete(String id) {
		if(eventoService.delete(Integer.parseInt(id)))
			System.out.println("Evento de id " + id + " deletado");
		else
			System.out.println("Não foi encontrado nenhum evento com este id");
	}

	public void edit(String id, String titulo, String descricao) {
		Evento e = eventoService.edit(Integer.parseInt(id), titulo, descricao);
		if(e != null)
			System.out.println("Evento de id " + id + " editado.");
		else
			System.out.println("Não foi possível encontrar evento.");
	}
	
}