package br.ufrn.imd.eventos.controllers;

import java.util.List;

import br.ufrn.imd.eventos.dominio.Usuario;
import br.ufrn.imd.eventos.dominio.Usuario;
import br.ufrn.imd.services.UsuarioService;

public class UsuarioMBean{

	private static UsuarioService usuarioService = new UsuarioService();
	
	private Usuario usuarioAtual;

	public UsuarioMBean(){
		Usuario a = new Usuario(1, "Sarah", "sarah" );
		Usuario b = new Usuario(2, "Pedro", "pedro");
		usuarioService.add(a);
		usuarioService.add(b);
	}
	
	public Usuario login(String username){
		return usuarioService.getByUsername(username);
	
		
	}
	public void add(String nome) {
		Usuario u = new Usuario(nome);
		System.out.println(u.getNome());
		usuarioService.add(u);
		System.out.println("Usuário " + u.getNome() + " criado com id " + u.getId());
	}

	public void list() {
		List<Usuario> Usuarios = usuarioService.listar();
		for (Usuario u : Usuarios) {
			System.out.println(u.getNome());
		}
	}
	
	public Boolean getById(String id){
		Usuario u = usuarioService.getById(Integer.parseInt(id));
		if(u != null){
			System.out.println("Usuário: " + u.getNome());
			return true;
		}
		else{
			System.out.println("Não foi possível encontrar Usuário com id " + id);
			return false;
		}
	}
	
	public void delete(String id) {
		if(usuarioService.delete(Integer.parseInt(id)))
			System.out.println("Usuário de id " + id + " deletado");
		else
			System.out.println("Não foi encontrado nenhum Usuário com este id");
	}

	public void edit(String id, String titulo, String descricao) {
		Usuario u = usuarioService.edit(Integer.parseInt(id), titulo, descricao);
		if(u != null)
			System.out.println("Usuário de id " + id + " editado.");
		else
			System.out.println("Não foi possível encontrar Usuário.");
	}
	
}