package br.ufrn.imd.eventos.dao;


import java.util.ArrayList;
import java.util.List;
import br.ufrn.imd.eventos.dominio.Evento;
import br.ufrn.imd.eventos.dominio.Usuario;


/**
 * DAO com consultas específicas da entidade Evento
 * 
 * @author sarah
 *
 */

public class EventoDao {

	private List<Evento> eventos = new ArrayList<Evento>();
	
	/*public List<Evento> listar() {
		//return getSession().createQuery("select e from Evento e").list();
	}*/
	
	public List<Evento> getAll() {
		//String hql = "select e from Evento e WHERE e.titulo like UPPER(:titulo)";
		return eventos;
	}
	
	public Boolean subscribe(Evento e, Usuario u){
		for (Evento ev : eventos) {
			if(ev.getId() == e.getId()){
				ev.addParticipante(u);
				return true;
			}
			return false;
		}
		return false;
	}

	public List<Evento> getByTitulo(String titulo) {
		// TODO Auto-generated method stub
		return null;
	}

	public void add(Evento e) {
		e.setId(eventos.size()+1);
		eventos.add(e);
		
	}

	public Evento getById(int id) {
	//	List<Evento> eventos = eventoDao.getAll();
		for (Evento e : eventos) {
			if(e.getId() == id)
				return e;
		}
		return null;
	}

	public Boolean delete(int id) {
		for (Evento e : eventos) {
			if(e.getId() == id){
				eventos.remove(e);
				return true;
			}
			return false;
		}
		return false;
	}

	public Evento edit(int id, String titulo, String descricao) {
		for (Evento e : eventos) {
			if(e.getId() == id){
				e.setTitulo(titulo);
				e.setDescricao(descricao);
				return e;
			}
		}
		return null;
		
	}
	
	


}
