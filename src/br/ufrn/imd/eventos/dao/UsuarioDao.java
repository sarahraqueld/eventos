package br.ufrn.imd.eventos.dao;


import java.util.ArrayList;
import java.util.List;
import br.ufrn.imd.eventos.dominio.Usuario;


/**
 * DAO com consultas específicas da entidade Evento
 * 
 * @author sarah
 *
 */

public class UsuarioDao {

	private List<Usuario> usuarios = new ArrayList<Usuario>();
	
	
	public List<Usuario> getAll() {
		//String hql = "select e from Evento e WHERE e.titulo like UPPER(:titulo)";
		return usuarios;
	}

	public List<Usuario> getByTitulo(String titulo) {
		// TODO Auto-generated method stub
		return null;
	}

	public void add(Usuario u) {
		u.setId(usuarios.size()+1);
		usuarios.add(u);
	}

	public Usuario getById(int id) {
	//	List<Evento> eventos = eventoDao.getAll();
		for (Usuario u : usuarios) {
			if(u.getId() == id)
				return u;
		}
		return null;
	}

	public Usuario getByUsername(String username) {
	//	List<Evento> eventos = eventoDao.getAll();
		for (Usuario u : usuarios) {
			if(u.getUsername().equals(username))
				return u;
		}
		return null;
	}
	
	public Boolean delete(int id) {
		for (Usuario e : usuarios) {
			if(e.getId() == id){
				usuarios.remove(e);
				return true;
			}
			return false;
		}
		return false;
		
	}

	public Usuario edit(int id, String titulo, String username) {
		for (Usuario u : usuarios) {
			if(u.getId() == id){
				u.setNome(titulo);
				u.setUsername(username);
				return u;
			}
		}
		return null;
		
	}
	
	


}
